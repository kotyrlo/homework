<?php

error_reporting(E_ALL);

require_once __DIR__ . '/lib.php';

$dir = __DIR__ . '/data';
$dir = normalizeDirName($dir);
$file = getArrayValue($_GET, 'file');
unlink("{$dir}/{$file}");
header('Location: /');
exit;
